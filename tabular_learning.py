import os 
import torch 
import numpy as np  
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable

def read_txt():
    with open("0.txt", "r") as f:
        lines = f.read().strip().splitlines()
    with open("1.txt", "r") as f1:
        lines1 = f1.read().strip().splitlines()
    with open("2.txt", "r") as f2:
        lines2 = f2.read().strip().splitlines()
    with open("3.txt", "r") as f3:
        lines3 = f3.read().strip().splitlines()
    with open("4.txt", "r") as f4:
        lines4 = f4.read().strip().splitlines()
    return  lines, lines1, lines2, lines3, lines4

def create_tensor(ndims = 58):
    
    lines, lines1, lines2, lines3, lines4 = read_txt()
    a = 0
    b = 0
    c = 0
    d = 0
    e = 0
    #### 1 tensort
    standing =  torch.zeros((1, ndims), dtype=torch.float32)
    for input in lines :
        input = input.replace("\t", ", ")
        list0 = list(map(float, input.split(",")))
        temp = torch.tensor(list0).reshape(1,58)
        if a == 0:
            standing = standing + temp
            a +=1
        else: 
            standing = torch.cat((standing,temp))
    #### 2 tensort
    siting =  torch.zeros((1, ndims), dtype=torch.float32)
    for input1 in lines1 :
        input1 = input1.replace("\t", ", ")
        list1 = list(map(float, input1.split(",")))
        temp1 = torch.tensor(list1).reshape(1,58)
        if b == 0:
            siting = siting + temp1
            b +=1
        else: 
            siting = torch.cat((siting,temp1))
    #### 3 tensort
    siting_on_bed =  torch.zeros((1, ndims), dtype=torch.float32)
    for input2 in lines2 :
        input2 = input2.replace("\t", ", ")
        list2 = list(map(float, input2.split(",")))
        temp2 = torch.tensor(list2).reshape(1,58)
        if c == 0:
            siting_on_bed = siting_on_bed + temp2
            c +=1
        else: 
            siting_on_bed = torch.cat((siting_on_bed,temp2))
    #### 4 tensort
    lying_on_bed =  torch.zeros((1, ndims), dtype=torch.float32)
    for input3 in lines3 :
        input3 = input3.replace("\t", ", ")
        list3 = list(map(float, input3.split(",")))
        temp3 = torch.tensor(list3).reshape(1,58)
        if d == 0:
            lying_on_bed = lying_on_bed + temp3
            d +=1
        else: 
            lying_on_bed = torch.cat((lying_on_bed,temp3))
    #### 5 tensort
    lying_on_floor =  torch.zeros((1, ndims), dtype=torch.float32)
    for input4 in lines4 :
        input4 = input4.replace("\t", ", ")
        list4 = list(map(float, input4.split(",")))
        temp4 = torch.tensor(list4).reshape(1,58)
        if e == 0:
            lying_on_floor = lying_on_floor + temp4
            e +=1
        else: 
            lying_on_floor = torch.cat((lying_on_floor,temp4))

    one_hot  = torch.zeros((len(standing),), dtype=int) #F.one_hot(torch.tensor([0]*i),4)
    one_hot = one_hot + torch.tensor(0)
    one_hot_train = one_hot[:int(len(one_hot)*0.8)]
    one_hot_vald = one_hot[int(len(one_hot)*0.8):]

    one_hot1 = torch.zeros((len(siting),), dtype=int)
    one_hot1 = one_hot1 + torch.tensor(1)
    one_hot1_train = one_hot1[:int(len(siting)*0.8)]
    one_hot1_vald = one_hot1[int(len(one_hot1)*0.8):]

    one_hot2 = torch.zeros((len(siting_on_bed),), dtype=int)
    one_hot2 = one_hot2 + torch.tensor(2)
    one_hot2_vald  = one_hot2[int(len(one_hot2)*0.8):]
    one_hot2_train = one_hot2[:int(len(one_hot2)*0.8)]
    
    one_hot3 = torch.zeros((len(lying_on_bed),), dtype=int)
    one_hot3 = one_hot3+ torch.tensor(3)
    one_hot3_train = one_hot3[:int(len(one_hot3)*0.8)]
    one_hot3_vald  = one_hot3[int(len(one_hot3)*0.8):]

    one_hot4 = torch.zeros((len(lying_on_floor),), dtype=int)
    one_hot4 = one_hot4+ torch.tensor(4)
    one_hot4_train = one_hot4[:int(len(one_hot4)*0.8)]
    one_hot4_vald  = one_hot4[int(len(one_hot4)*0.8):]


    FINAL_TENSOR_train = torch.zeros((1, ndims), dtype=torch.float32)
    FINAL_TENSOR_train = FINAL_TENSOR_train+ standing[:int(len(standing)*0.8)]
    FINAL_TENSOR_train = torch.cat((FINAL_TENSOR_train,siting[:int(len(siting)*0.8)]))
    FINAL_TENSOR_train = torch.cat((FINAL_TENSOR_train,siting_on_bed[:int(len(siting_on_bed)*0.8)]))
    FINAL_TENSOR_train = torch.cat((FINAL_TENSOR_train,lying_on_bed[:int(len(lying_on_bed)*0.8)]))
    FINAL_TENSOR_train = torch.cat((FINAL_TENSOR_train,lying_on_floor[:int(len(lying_on_floor)*0.8)]))

    FINAL_TENSOR_valid = torch.zeros((1, ndims), dtype=torch.float32)
    FINAL_TENSOR_valid = FINAL_TENSOR_valid+ standing[int(len(standing)*0.8):]
    FINAL_TENSOR_valid = torch.cat((FINAL_TENSOR_valid,siting[int(len(siting)*0.8):]))
    FINAL_TENSOR_valid = torch.cat((FINAL_TENSOR_valid,siting_on_bed[int(len(siting_on_bed)*0.8):]))
    FINAL_TENSOR_valid = torch.cat((FINAL_TENSOR_valid,lying_on_bed[int(len(lying_on_bed)*0.8):]))
    FINAL_TENSOR_valid = torch.cat((FINAL_TENSOR_valid,lying_on_floor[int(len(lying_on_floor)*0.8):]))

    FINAL_ONE_HOT_train = torch.zeros((1,), dtype=torch.int)
    FINAL_ONE_HOT_train = FINAL_ONE_HOT_train+ one_hot_train
    FINAL_ONE_HOT_train = torch.cat((FINAL_ONE_HOT_train,one_hot1_train))
    FINAL_ONE_HOT_train = torch.cat((FINAL_ONE_HOT_train,one_hot2_train))
    FINAL_ONE_HOT_train = torch.cat((FINAL_ONE_HOT_train,one_hot3_train))
    FINAL_ONE_HOT_train = torch.cat((FINAL_ONE_HOT_train,one_hot4_train))

    FINAL_ONE_HOT_valid = torch.zeros((1,), dtype=torch.int)
    FINAL_ONE_HOT_valid = FINAL_ONE_HOT_valid+ one_hot_vald
    FINAL_ONE_HOT_valid = torch.cat((FINAL_ONE_HOT_valid, one_hot1_vald))
    FINAL_ONE_HOT_valid = torch.cat((FINAL_ONE_HOT_valid, one_hot2_vald))
    FINAL_ONE_HOT_valid = torch.cat((FINAL_ONE_HOT_valid, one_hot3_vald))
    FINAL_ONE_HOT_valid = torch.cat((FINAL_ONE_HOT_valid, one_hot4_vald))

    return FINAL_ONE_HOT_train, FINAL_ONE_HOT_valid ,  FINAL_TENSOR_train, FINAL_TENSOR_valid


class Net(nn.Module):
    def __init__(self,cols,size_hidden,classes):
        super(Net, self).__init__()
    #     #Note that 17 is the number of columns in the input matrix. 
    #     self.fc1 = nn.Linear(cols, size_hidden)
    #     #variety of # possible for hidden layer size is arbitrary, but needs to be consistent across layers.  3 is the number of classes in the output (died/survived)
    #     self.fc2 = nn.Linear(size_hidden, size_hidden)
        
    #     self.fc3 = nn.Linear(size_hidden, classes)
        
    # def forward(self, x):
    #     x = self.fc1(x)
    #     # x = F.dropout(x, p=0.1)
    #     x = F.relu(x)
    #     x = self.fc2(x)
    #     x = self.fc3(x)
    #     return F.softmax(x, dim=1)
        self.fc1 = nn.Linear(cols, size_hidden)
        self.bn1 = nn.BatchNorm1d(size_hidden)
        self.fc2 = nn.Linear(size_hidden, size_hidden)    
        self.bn2 = nn.BatchNorm1d(size_hidden)
        self.fc3 = nn.Linear(size_hidden, size_hidden) 
        self.bn3 = nn.BatchNorm1d(size_hidden)
        self.fc4 = nn.Linear(size_hidden, classes) 

    def forward(self,x):
        # x = F.relu(self.bn1(self.fc1(x)))
        # x = F.relu(self.bn2(self.fc2(x)))
        # x = F.relu(self.bn3(self.fc3(x)))
        x = F.relu((self.fc1(x)))
        x = F.relu((self.fc2(x)))
        x = F.relu((self.fc3(x)))
        x = self.fc4(x)
        return F.log_softmax(x,dim=1)

from sklearn.utils import shuffle
import pandas as pd
#This is a little bit tricky to get the resulting prediction
def test(model, X_test):
    preds = []
    a = [0.9102, 0.6852, 0.1653, 0.9492, 0.7189, 0.1863, 0.9232, 0.7525, 0.2107,
        0.9492, 0.7189, 0.4749, 0.9492, 0.8872, 0.3672, 0.8711, 0.7525, 0.7713,
        0.8711, 0.7862, 0.5500, 0.6497, 0.5842, 0.5298, 0.6497, 0.6179, 0.3918,
        0.4935, 0.4832, 0.5908, 0.4935, 0.5168, 0.4029, 0.4805, 0.7525, 0.6053,
        0.4935, 0.8872, 0.3663, 0.1549, 0.2811, 0.5423, 0.1419, 0.3821, 0.1744,
        0.1159, 0.3148, 0.3186, 0.1289, 0.3821, 0.1921, 0.0098, 0.0218, 0.0063,
        0.0047, 0.9851, 0.0044, 0.0028]
    b = torch.tensor(a)
    
    with torch.no_grad():
        # import ipdb; ipdb.set_trace()
        b = b.unsqueeze(0)
        y_hat = model.forward(b)
        # for val in X_test:
        #     val = val.unsqueeze(0)
        #     y_hat = model.forward(val)
        preds.append(y_hat.argmax().item()) 
    return preds

if __name__=="__main__":
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Executing the model on :",device)
    batch_size = 200
    num_epochs = 100
    learning_rate = 0.001    
    size_hidden= 64        
    running_loss = 0.0
    FINAL_ONE_HOT_train, FINAL_ONE_HOT_valid ,FINAL_TENSOR_train, FINAL_TENSOR_valid = create_tensor()
    # 

    batch_no = len(FINAL_TENSOR_train) // batch_size  #batches
    classes = len(np.unique(FINAL_ONE_HOT_train))
    cols=FINAL_TENSOR_train.shape[1]
    net = Net(cols, size_hidden, classes)
    optimizer = torch.optim.Adam(net.parameters(), lr=learning_rate)
    criterion = nn.CrossEntropyLoss()

    for epoch in range(num_epochs):
        #Shuffle just mixes up the dataset between epocs
        # import ipdb; ipdb.set_trace()
        train_X, train_y = shuffle(FINAL_TENSOR_train, FINAL_ONE_HOT_train)
    # # Mini batch learning
        for i in range(batch_no):
            start = i * batch_size
            end = start + batch_size
            inputs = Variable(torch.FloatTensor(train_X[start:end]))
            labels = Variable(torch.LongTensor(train_y[start:end]))
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()

        print('Epoch {}'.format(epoch+1), "loss: ",running_loss)
        running_loss = 0.0

    with torch.no_grad():
        net.eval()
        traced = torch.jit.trace(net, torch.FloatTensor(train_X[start:end]))
        traced.save("tabular_v1.jit")

    state_dict = net.state_dict()
    torch.save(state_dict, "tabular_v1.pt")

    # result1=calculate_accuracy(train_X,train_y)
    # result2=calculate_accuracy(FINAL_TENSOR_valid,FINAL_ONE_HOT_valid)

    predict = test(net, FINAL_TENSOR_valid)
    print(predict)