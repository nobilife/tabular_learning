from tabular_learning import *
from tqdm import tqdm
class infer_():
    def __init__(self):
        self.size_hidden = 64
        self.classes = 5
        self.device = torch.device('cuda:0')
        self.FINAL_ONE_HOT_train, self.FINAL_ONE_HOT_valid,  self.FINAL_TENSOR_train, self.FINAL_TENSOR_valid =create_tensor(ndims=58)
        self.cols=self.FINAL_TENSOR_train.shape[1]
        self.model = Net(self.cols, self.size_hidden, self.classes)
        self.model.load_state_dict(torch.load("tabular_v1.pt", map_location=self.device))
        self.model.eval()
        
    def process(self):
        count = 0
        for i in tqdm(range(len(self.FINAL_TENSOR_valid))):
            self.x = self.FINAL_TENSOR_valid[i]
            with torch.no_grad():
                self.x = self.x.unsqueeze(0)
                self.result = self.model.forward(self.x)
                self.result = self.result.argmax().item()
                # import ipdb; ipdb.set_trace()
                # print(self.FINAL_ONE_HOT_valid.numpy()[i])
                if self.result != self.FINAL_ONE_HOT_valid.numpy()[i]:
                    count += 1
        print("True :", len(self.FINAL_TENSOR_valid)- count, " Wrong :", count)

if __name__=="__main__":
    infer = infer_()
    infer.process()